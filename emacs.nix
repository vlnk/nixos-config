{ pkgs ? import <nixpkgs> {} }:

let
  e = pkgs.emacs;
  eWithPackages = (pkgs.emacsPackagesNgGen e).emacsWithPackages;
in
  eWithPackages (epkgs: (with epkgs.melpaStablePackages; [
    magit # Integrate git <C-x g>
    cider # ClosureScript support
  ]) ++ (with epkgs.melpaPackages; [
    spacemacs-theme
    telephone-line
  ]) ++ (with epkgs.elpaPackages; [
    beacon # Highlight my cursor when scrolling
    nameless # Hide current package name everywhere in elisp code
  ]))
