{ config, pkgs, ... }:

{ imports = [ ./bob-hardware.nix ./common.nix ];

  users.users.vlnk =
  { isNormalUser = true;
    home = "/home/vlnk";
    extraGroups = ["wheel" "networkmanager"];
  };

  system.autoUpgrade.enable = true;
  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs;
  [ # common
    curl
    git

    # xorg
    xorg.xmodmap

    # cli
    exa

    # gui
    firefox
    vscode

    # dev
    nodejs
    rustup
    python3
    pipenv
    luarocks
  ];

  programs =
  { bash.enableCompletion = true;
    mtr.enable = true;
    gnupg.agent = { enable = true; enableSSHSupport = true; };
  };

  services =
  { # ssh
    openssh.enable = true;

    xserver = {
      enable = true;

      # layout
      layout = "us";
      xkbOptions = "eurosign:e";

      # displayMgr
      displayManager.lightdm.enable = true;

      # windowMgr
      windowManager = {
        awesome.enable = true;
        default = "awesome";
      };
    };
  };
}
