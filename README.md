# Desktop Configuration
+ [NixOs Manuel](https://nixos.org/nixos/manual/index.html)
+ [My First Awesome](https://awesomewm.org/apidoc/documentation/07-my-first-awesome.md.html)
+ [Awesome Ideas](https://github.com/awesomeWM/awesome/issues/1395)
+ [GNU Stow](http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html)
+ [Cli Ideas](https://remysharp.com/2018/08/23/cli-improved)
+ [tmux Courses](https://thoughtbot.com/upcase/tmux)
+ [i3 User Guide](https://i3wm.org/docs/userguide.html)
+ [Emacs User Guide](https://www.gnu.org/software/emacs/manual/html_node/emacs/Intro.html)
+ [Spacemacs Getting Started](http://spacemacs.org/doc/QUICK_START.html)
+ [Dotfiles Explanation](http://mywiki.wooledge.org/DotFiles)

## Update all OS configuration
It is not the "how I will build my OS from scratch", it works only into the NixOS itself.

```bash
NIXOS_CONFIG="/etc/nixos/configuration.nix"

sudo cp *.nix /etc/nixos/
[ -f $NIXOS_CONFIG ] && sudo rm $NIXOS_CONFIG
sudo ln -s /etc/nixos/tron.nix $NIXOS_CONFIG

nixos-rebuild switch --show-trace

# Or if you are busy
sudo ./switch.sh
```

## Upgrade NixOS
Everyone want you to check the [Release notes](https://nixos.org/nixos/manual/release-notes.html) if it is fine. So, do it.

```bash
# check the release notes
nix-channel --add https://nixos.org/channels/nixos-<VERSION> nixos
nixos-rebuild --upgrade boot

# when you are ready
reboot
```

## Clean your build
The dependencies changes, you removed this or that, you want to use autoremove...

```bash
nix-collect-garbage -d
```

## Update python's nix-env environment
Finally, I've read the [Nix Python Guide](https://nixos.org/nixpkgs/manual/#python) and I've found a right solution for myself.

```bash
  # In case I would like to update my python environment
  nix-env -if python.nix
```
