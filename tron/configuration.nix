{ config, pkgs, ... }:

{
  imports = [
    ./hardware.nix
  ];

  # GRUB2 configuration
  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda";
  };

  # Network configuration
  networking = {
    hostName = "tron";

    firewall.enable = false;

    networkmanager = {
      enable = true;
      wifi.powersave = true;
    };
  };

  # Enable ssh
  services.openssh = {
    enable = true;
    authorizedKeysFiles = [".ssh/authorized_keys"];
    # startWhenNeeded = true;
  };

  # Enable GNOME keyring
  security.pam = {
    services.lightdm.enableGnomeKeyring = true;
  };

  # Internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    console.keyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # Time zone
  time.timeZone = "America/Montreal";

   # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound.
  sound.enable = true;

  # Enable bluetooth
  services.blueman.enable = true;

  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
  };

  # Enable bluetooth
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
  };

  # Enable docker
  virtualisation.docker.enable = true;

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;

    # Keyboard layout
    layout = "us";
    xkbOptions = "eurosign:e";

    # Enable touchpad support.
    libinput.enable = true;

    displayManager = {
      lightdm.enable = true;
    };

    windowManager.i3.enable = true;
  };

  services.teamviewer.enable = true;

  programs = {
    bash.enableCompletion = true;
    mtr.enable = true;
    fish.enable = true;

    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };

  users.users.vlnk = {
    isNormalUser = true;
    home = "/home/vlnk";
    extraGroups = ["wheel" "networkmanager" "docker"];
  };

  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    curl gnumake git # common
    firefox thunderbird vlc # gui
    gimp imagemagick # images
    vscodium spotify # non-free
  ] ++ (with pkgs.gnome3; [
    nautilus # gnome3
  ]);

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system = {
    stateVersion = "19.09"; # Did you read the comment?
    autoUpgrade.enable = true;
  };
}
