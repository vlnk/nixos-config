NIXOS_CONFIG="/etc/nixos/configuration.nix"

sudo cp tron/*.nix /etc/nixos/
[ -f $NIXOS_CONFIG ] && sudo rm $NIXOS_CONFIG
sudo ln -s /etc/nixos/tron.nix $NIXOS_CONFIG

nixos-rebuild switch --show-trace
